## Overview
Serilog.Exceptions.MongoDb is a Mongo exception destructurer for the [Serilog.Exceptions](https://github.com/RehanSaeed/Serilog.Exceptions) project.

## Getting Started

```
Install-Package Serilog.Exceptions.MongoDb
```

Add the `MongoExceptionDestructurer` during setup:
```csharp
var destructurerOptions = new DestructuringOptionsBuilder()
	.WithDefaultDestructurers()
	.WithDestructurers(new[] { new MongoExceptionDestructurer()});
				
_logger = new LoggerConfiguration()
	.Enrich.WithExceptionDetails(destructurerOptions)
	.WriteTo.RollingFile(
        new JsonFormatter(renderMessage: true), 
        @"C:\logs\log-{Date}.txt")    
    .CreateLogger();
```
﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Clusters;
using MongoDB.Driver.Core.Connections;
using MongoDB.Driver.Core.Servers;
using System.Collections;
using System.Collections.Generic;
using System.Net;

namespace Serilog.Exceptions.MongoDb.Tests.TestData
{
    public class MongoExceptionsAllTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            return MongoExceptionTestData.All.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class MongoExceptionsClientIdTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            return MongoExceptionTestData.WithConnectionId.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public static class MongoExceptionTestData
    {
        public static string Message => "MongoExceptionTest";
        public static ClusterId ClusterId => new ClusterId(1);
        public static ClusterConnectionMode ClusterConnectionMode => new ClusterConnectionMode();
        public static IPEndPoint EndPoint => new IPEndPoint(2130706433, 27017);
        public static ServerId ServerId => new ServerId(ClusterId, EndPoint);
        public static ServerDescription ServerDescription => new ServerDescription(ServerId, EndPoint);
        public static IEnumerable<ServerDescription> Servers => new List<ServerDescription> { ServerDescription };
        public static ClusterDescription ClusterDescription => new ClusterDescription(ClusterId, ClusterConnectionMode, ClusterType.ReplicaSet, Servers);
        public static int ServerValue => 7;
        public static ConnectionId ConnectionId => new ConnectionId(ServerId, 7).WithServerValue(ServerValue);
        public static BsonDocument Command => new BsonDocument("SomeCommandName", "SomeCommandValue");
        public static BsonDocument Result => new BsonDocument
        {
            { "Code", 11000 },
            { "CodeName", "DuplicateKey" },
            { "ErrorMessage", "E11000 duplicate key error collection: MyDatabase.SomeCollection" }
        };
        public static BsonDocument Query => new BsonDocument("SomeQueryName", "SomeQueryValue");
        public static BsonDocument QueryResult => new BsonDocument("SomeQueryResultName", "SomeQueryResultValue");
        public static IEnumerable<BulkWriteError> BulkWriteErrors => new List<BulkWriteError>();
        public static BulkWriteResult<string> BulkWriteResult = new BulkWriteResult<string>.Acknowledged(0, 0, 0, 0, 0, new List<WriteModel<string>>(), new List<BulkWriteUpsert>());
        public static IEnumerable<WriteModel<string>> WriteModels = new List<WriteModel<string>>();

        public static IEnumerable<object[]> All
        {
            get
            {
                var all = new List<object[]>();

                all.AddRange(General);
                all.AddRange(WithConnectionId);

                return all;
            }
        }

        public static IEnumerable<object[]> General => new List<object[]>
        {
            new object[] { new MongoException(Message) },
            new object[] { new MongoClientException(Message) },
            new object[] { new MongoConfigurationException(Message) },
            new object[] { new MongoIncompatibleDriverException(ClusterDescription) },
            new object[] { new MongoWaitQueueFullException(Message) },
            new object[] { new MongoInternalException(Message) }
        };

        public static IEnumerable<object[]> WithConnectionId => new List<object[]>
        {
            new object[] { new MongoConnectionException(ConnectionId, Message) },
            new object[] { new MongoAuthenticationException(ConnectionId, Message) },
            new object[] { new MongoConnectionClosedException(ConnectionId) },
            new object[] { new MongoServerException(ConnectionId, Message) },
            new object[] { new MongoBulkWriteException<string>(ConnectionId, BulkWriteResult, BulkWriteErrors, null, WriteModels) },
            new object[] { new MongoCommandException(ConnectionId, Message, null, null) },
            new object[] { new MongoCommandException(ConnectionId, Message, Command, Result) },
            new object[] { new MongoExecutionTimeoutException(ConnectionId, Message, Result) },
            new object[] { new MongoQueryException(ConnectionId, Message, null, null) },
            new object[] { new MongoQueryException(ConnectionId, Message, Query, QueryResult) },
            new object[] { new MongoCursorNotFoundException(ConnectionId, 1, Query) },
            new object[] { new MongoWriteException(ConnectionId, null, null, null) }
        };
    }
}

﻿using MongoDB.Driver;
using Serilog.Exceptions.Core;
using Serilog.Exceptions.MongoDb.Destructurers;
using Serilog.Exceptions.MongoDb.Tests.TestData;
using Serilog.Exceptions.MongoDb.Tests.Tools;
using Serilog.Sinks.TestCorrelator;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Serilog.Exceptions.MongoDb.Tests.Destructurers
{
    public class MongoExceptionDestructurerTests
    {
        private readonly Serilog.Core.Logger _logger;

        public MongoExceptionDestructurerTests()
        {
            var destructurerOptions = new DestructuringOptionsBuilder()
                .WithDefaultDestructurers()
                .WithDestructurers(new[] { new MongoExceptionDestructurer() });

            var loggerConfiguration = new LoggerConfiguration()
                .Enrich.WithExceptionDetails(destructurerOptions)
                .WriteTo.TestCorrelator();

            _logger = loggerConfiguration.CreateLogger();
        }

        [Theory()]
        [ClassData(typeof(MongoExceptionsAllTestData))]
        public void Given_MongoException_When_Logging_Then_ContainsErrorLabels(MongoException exception)
        {
            using (TestCorrelator.CreateContext())
            {
                // Arrange                
                var errorLabel = "some error";
                exception.AddErrorLabel(errorLabel);

                // Act
                _logger.Error(exception, "test");

                // Assert
                var exceptionProperties = LogUtils.GetExceptionsWithExceptionDetailProperties().FirstOrDefault();
                Assert.Contains(nameof(MongoException.ErrorLabels), exceptionProperties);
                Assert.Contains(errorLabel, (IEnumerable<object>)exceptionProperties[nameof(MongoException.ErrorLabels)]);
            }
        }

        [Theory()]
        [ClassData(typeof(MongoExceptionsClientIdTestData))]
        public void Given_MongoExceptionWithConnectionId_When_Logging_Then_ContainsConnectionId(MongoException exception)
        {
            using (TestCorrelator.CreateContext())
            {
                // Arrange
                var connectionId = MongoExceptionTestData.ConnectionId;
                var property = nameof(MongoConnectionException.ConnectionId);

                // Act
                _logger.Error(exception, "test");

                // Assert
                var exceptionProperties = LogUtils.GetExceptionsWithExceptionDetailProperties().First();
                if (!exceptionProperties.ContainsKey(property))
                    return;

                if (exceptionProperties[property] is IDictionary<string, object> propDict)
                {
                    Assert.Equal(MongoExceptionTestData.ConnectionId.LocalValue, propDict[nameof(MongoExceptionTestData.ConnectionId.LocalValue)]);
                    Assert.NotNull(propDict[nameof(MongoExceptionTestData.ConnectionId.ServerId)]);
                    Assert.Equal(MongoExceptionTestData.ConnectionId.ServerValue, propDict[nameof(MongoExceptionTestData.ConnectionId.ServerValue)]);
                }
            }
        }
    }
}

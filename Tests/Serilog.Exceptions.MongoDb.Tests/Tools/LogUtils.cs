﻿using Serilog.Events;
using Serilog.Sinks.TestCorrelator;
using System.Collections.Generic;
using System.Linq;

namespace Serilog.Exceptions.MongoDb.Tests.Tools
{
    public static class LogUtils
    {
        /// <summary>
        /// Iterates over all of the logged exceptions and extracts the properties for the ExceptionDetail key.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<IDictionary<object, object>> GetExceptionsWithExceptionDetailProperties()
        {
            var exceptionsWithProperties = new List<IDictionary<object, object>>();
            var logEvents = TestCorrelator.GetLogEventsFromCurrentContext();

            foreach (var logEvent in logEvents)
            {
                if (logEvent.Properties.TryGetValue("ExceptionDetail", out var logEventPropertyValue))
                {
                    if (logEventPropertyValue is DictionaryValue propertyDict)
                    {
                        exceptionsWithProperties.Add(propertyDict.Elements.ToDictionary(kv => kv.Key.Value, kv => DestructurePropertyValue(kv.Value)));
                    }
                }
            }

            return exceptionsWithProperties;
        }

        /// <summary>
        /// Recursively destructures a <see cref="LogEventPropertyValue"/> into a flatter model.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static object DestructurePropertyValue(this LogEventPropertyValue input)
        {
            switch (input)
            {
                case ScalarValue scalar:
                    return scalar.Value;
                case DictionaryValue dictionary:
                    return dictionary.Elements.ToDictionary(kv => kv.Key.Value, kv => DestructurePropertyValue(kv.Value));
                case SequenceValue sequence:
                    return sequence.Elements.Select(DestructurePropertyValue).ToList();
                case StructureValue structure:
                    return structure.Properties.ToDictionary(p => p.Name, p => DestructurePropertyValue(p.Value));
                default:
                    return null;
            }
        }
    }
}

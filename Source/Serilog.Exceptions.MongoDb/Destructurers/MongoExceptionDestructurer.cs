﻿using MongoDB.Driver;
using Serilog.Exceptions.Core;
using Serilog.Exceptions.Destructurers;
using System;
using System.Collections.Generic;

namespace Serilog.Exceptions.MongoDb.Destructurers
{
    /// <summary>
    /// A destructurer for <see cref="MongoException"/>.
    /// </summary>
    /// <seealso cref="ExceptionDestructurer" />
    public class MongoExceptionDestructurer : ExceptionDestructurer
    {
        /// <inheritdoc />
        public override Type[] TargetTypes => new[] {
            typeof(MongoException),
            typeof(MongoClientException),
            typeof(MongoConfigurationException),
            typeof(MongoIncompatibleDriverException),
            typeof(MongoWaitQueueFullException),
            typeof(MongoConnectionException),
            typeof(MongoAuthenticationException),
            typeof(MongoConnectionClosedException),
            typeof(MongoInternalException),
            typeof(MongoServerException),
            typeof(MongoBulkWriteException),
            typeof(MongoCommandException),
            typeof(MongoExecutionTimeoutException),
            typeof(MongoQueryException),
            typeof(MongoCursorNotFoundException),
            typeof(MongoWriteException)
        };

        /// <inheritdoc />
        public override void Destructure(
            Exception exception,
            IExceptionPropertiesBag propertiesBag,
            Func<Exception, IReadOnlyDictionary<string, object>> destructureException)
        {
            base.Destructure(exception, propertiesBag, destructureException);

            if (!(exception is MongoException mongoException))
                return;

            propertiesBag.AddProperty(nameof(mongoException.ErrorLabels), mongoException.ErrorLabels);

            if (exception is MongoConnectionException mongoConnectionException)
            {
                propertiesBag.AddProperty(nameof(mongoConnectionException.ConnectionId), mongoConnectionException.ConnectionId);
            }

            else if (exception is MongoServerException mongoServerException)
            {
                propertiesBag.AddProperty(nameof(mongoServerException.ConnectionId), mongoServerException.ConnectionId);

                if (exception is MongoCommandException mongoCommandException)
                {
                    propertiesBag.AddProperty(nameof(mongoCommandException.Command), mongoCommandException.Command?.ToString());

                    if (!(mongoCommandException.Result is null))
                    {
                        propertiesBag.AddProperty(nameof(mongoCommandException.Code), mongoCommandException.Code);
                        propertiesBag.AddProperty(nameof(mongoCommandException.CodeName), mongoCommandException.CodeName);
                        propertiesBag.AddProperty(nameof(mongoCommandException.ErrorMessage), mongoCommandException.ErrorMessage);
                        propertiesBag.AddProperty(nameof(mongoCommandException.Result), mongoCommandException.Result.ToString());
                    }
                }

                else if (exception is MongoExecutionTimeoutException mongoExecutionTimeoutException)
                {
                    propertiesBag.AddProperty(nameof(mongoExecutionTimeoutException.Code), mongoExecutionTimeoutException.Code);
                    propertiesBag.AddProperty(nameof(mongoExecutionTimeoutException.CodeName), mongoExecutionTimeoutException.CodeName);
                }

                else if (exception is MongoQueryException mongoQueryException)
                {
                    propertiesBag.AddProperty(nameof(mongoQueryException.Query), mongoQueryException.Query?.ToString());
                    propertiesBag.AddProperty(nameof(mongoQueryException.QueryResult), mongoQueryException.QueryResult?.ToString());

                    if (exception is MongoCursorNotFoundException mongoCursorNotFoundException)
                    {
                        propertiesBag.AddProperty(nameof(mongoCursorNotFoundException.CursorId), mongoCursorNotFoundException.CursorId);
                    }
                }
            }
        }
    }
}
